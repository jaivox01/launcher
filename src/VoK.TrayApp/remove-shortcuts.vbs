Set oShell = CreateObject("WScript.Shell")

strStartMenu = oShell.SpecialFolders("Programs")
strVoKLnkName = "\The Vault of Kundarak.lnk"
strStartMenuPath = strStartMenu + "\Vault of Kundarak"
strStartUpPath = strStartMenu + "\Startup"

Set FSO = CreateObject("Scripting.FileSystemObject")

If (FSO.FolderExists(strStartMenuPath)) Then
	FSO.DeleteFolder(strStartMenuPath)
End If

If (FSO.FileExists(strStartUpPath + strVoKLnkName)) Then
	FSO.DeleteFile(strStartUpPath + strVoKLnkName)
End If
