Set oShell = CreateObject("WScript.Shell")

strAppData = oShell.SpecialFolders("AppData")
strStartMenu = oShell.SpecialFolders("Programs")
strVoKPath = strAppData + "\VoK"
strVoKLnkName = "\The Vault of Kundarak.lnk"
strStartMenuPath = strStartMenu + "\Vault of Kundarak"
strStartUpPath = strStartMenu + "\Startup"

Set FSO = CreateObject("Scripting.FileSystemObject")

If NOT (FSO.FolderExists(strStartMenuPath)) Then
	FSO.CreateFolder(strStartMenuPath)
End If

Set oStartMenuShortcut = oShell.CreateShortcut(strStartMenuPath + strVoKLnkName)
oStartMenuShortcut.WindowStyle = 4
oStartMenuShortcut.TargetPath = strVoKPath+"\The Vault of Kundarak.exe"
oStartMenuShortcut.WorkingDirectory = strVoKPath
oStartMenuShortcut.Save

Set oStartUpShortcut = oShell.CreateShortcut(strStartUpPath + strVoKLnkName)
oStartUpShortcut.WindowStyle = 4
oStartUpShortcut.TargetPath = strVoKPath+"\The Vault of Kundarak.exe"
oStartUpShortcut.WorkingDirectory = strVoKPath
oStartUpShortcut.Save