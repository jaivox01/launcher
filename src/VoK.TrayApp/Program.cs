﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using log4net;
using Newtonsoft.Json;
using VoK.ServiceClients;

namespace VoK.TrayApp
{
    static class Program
    {
        public static TrayForm TrayIcon;

        public static MainForm MainForm;

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));
                Thread.CurrentThread.Name = "VoKMain";

                var processName = Process.GetCurrentProcess().ProcessName;

                if (Process.GetProcesses().Count(p => p.ProcessName == processName) > 1)
                {
                    // attempt to send it a message to show the ui... someday.  should probably be
                    // done via named pipes and that's just more than we want to deal with.
                    _log.Debug("Duplicate VoK application started - Highlander enabled.");
                    TextOutputWindow.ShowDialog("The Vault of Kundarak is already running, please open it from the system tray.", "OK");

                    return;
                }

                _log.Debug("VoK starting...");

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                string appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string mainSettingsPath = Path.Combine(appDataFolder, "VoK\\settings.json");

                if (!File.Exists(mainSettingsPath))
                {
                    // generate a new settings file:
                    Settings newSettings = new Settings();
                    File.WriteAllText(mainSettingsPath, JsonConvert.SerializeObject(newSettings, Formatting.Indented));
                    newSettings = null;
                }

                SettingsFactoryHost.DefaultSettingsFactory = new SettingsFactory(mainSettingsPath);
                ServiceClientHost.Instance = new TurbineServiceClient(SettingsFactoryHost.DefaultSettingsFactory);

                MainForm = new MainForm();

                if (args == null || args.Length < 1 || args[0] != "-silent")
                    MainForm.Show();

                using (TrayIcon = new TrayForm())
                {
                    Application.Run();
                }
            }
            catch (Exception ex)
            {
                _log.Error("Fatal exception in VoK", ex);
            }
        }

        public static void Exit()
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            TrayIcon.Visible = false;
            Application.Exit();
        }
    }
}
