﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Newtonsoft.Json;

namespace VoK
{
    public class Settings
    {
        [JsonProperty("ssgCidrBlocks")]
        public List<string> SsgCidrBlocks { get; set; }

        [JsonIgnore]
        private string _ddoServiceInfoUrl { get; set; }
        [JsonProperty("ddoServiceInfoUrl", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue("http://gls.ddo.com/GLS.DataCenterServer/Service.asmx")]
        public string DdoServiceInfoUrl
        {
            get => string.IsNullOrWhiteSpace(_ddoServiceInfoUrl) ? "http://gls.ddo.com/GLS.DataCenterServer/Service.asmx" : _ddoServiceInfoUrl;
            set => _ddoServiceInfoUrl = value;
        }

        [JsonIgnore]
        private string _lotroServiceInfoUrl { get; set; }
        [JsonProperty("lotroServiceInfoUrl", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue("http://gls.lotro.com/GLS.DataCenterServer/Service.asmx")]
        public string LotroServiceInfoUrl
        {
            get => string.IsNullOrWhiteSpace(_lotroServiceInfoUrl) ? "http://gls.lotro.com/GLS.DataCenterServer/Service.asmx" : _lotroServiceInfoUrl;
            set => _lotroServiceInfoUrl = value;
        }

        [JsonIgnore]
        private string _authServiceUrl { get; set; }
        /// <summary>
        /// ddo or lotro.com, same service under the hood
        /// </summary>
        [JsonProperty("authServiceUrl", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue("https://gls-auth.ddo.com/GLS.AuthServer/Service.asmx")]
        public string AuthServiceUrl
        {
            get => string.IsNullOrWhiteSpace(_authServiceUrl) ? "https://gls-auth.ddo.com/GLS.AuthServer/Service.asmx" : _authServiceUrl;
            set => _authServiceUrl = value;
        }

        [JsonProperty("enableLogging", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(false)]
        public bool EnableLogging { get; set; }

        [JsonProperty("ddoPath")]
        public string DdoPath { get; set; }

        [JsonProperty("lotroPath")]
        public string LotroPath { get; set; }

        [JsonProperty("keepRawData", DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(false)]
        public bool KeepRawData { get; set; }

        [JsonProperty("dataExportPath")]
        public string DataExportPath { get; set; }

        [JsonProperty("storedAccounts")]
        public List<StoredAccount> StoredAccounts { get; set; } = new List<StoredAccount>();

        [JsonProperty("patchBeforeLaunch")]
        public bool PatchBeforeLaunch { get; set; } = false;

        [JsonProperty("enableNotifications")]
        [DefaultValue(true)]
        public bool EnableNotifications { get; set; } = true;

        [JsonProperty("use64BitClients")]
        [DefaultValue(false)]
        public bool Use64BitClients { get; set; } = false;

        [JsonProperty("checkForPatchMinutes")]
        [DefaultValue(null)]
        public int? CheckForPatchMinutes { get; set; }

        [JsonProperty("minimizeBeforeLaunch")]
        [DefaultValue(false)]
        public bool MinimizeBeforeLaunch { get; set; }
    }
}
