﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoK
{
    public class Shortcut
    {
        [JsonProperty("server")]
        public string Server { get; set; }

        [JsonProperty("character")]
        public string Character { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        public string GetDisplayName()
        {
            if (!string.IsNullOrWhiteSpace(DisplayName))
                return DisplayName;

            return Server + (!string.IsNullOrWhiteSpace(Character) ? " - " + Character : "");
        }
    }
}
