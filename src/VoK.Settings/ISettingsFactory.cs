﻿using System;

namespace VoK
{
    public interface ISettingsFactory
    {
        Settings GetCurrentSettings();

        void SaveSettings(Settings settings);

        event EventHandler OnChange;
    }
}