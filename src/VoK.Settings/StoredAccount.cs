﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VoK
{
    public class StoredAccount
    {
        private byte[] encryptionSalt;

        public StoredAccount()
        {
            // generate some entropic data in case this is a new account.  worst case
            // it just gets thrown away later
            Random rnd = new Random();
            encryptionSalt = new byte[24];
            rnd.NextBytes(encryptionSalt);
        }

        [JsonProperty("salt")]
        public string Salt
        {
            get { return string.Concat(encryptionSalt.Select(b => b.ToString("X2")).ToArray()); }
            set { encryptionSalt = StringToByteArray(value); }
        }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("encryptedPassword")]
        public string EncryptedPassword { get; set; }

        [JsonIgnore]
        public string DecryptedPassword
        {
            get
            {
                return DecryptString(EncryptedPassword);
            }
            set
            {
                EncryptedPassword = EncryptString(value);
            }
        }

        /// <summary>
        /// should be IReadOnlyList, but whatever.
        /// </summary>
        [JsonProperty("subscriptions")]
        public List<Subscription> Subscriptions { get; set; } = new List<Subscription>();

        private string EncryptString(string clearText)
        {
            byte[] decryptedTextBytes = ASCIIEncoding.ASCII.GetBytes(clearText);
            byte[] encryptedTextBytes = ProtectedData.Protect(decryptedTextBytes, encryptionSalt, DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedTextBytes);
        }

        private string DecryptString(string encryptedText)
        {
            try
            {
                byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);
                byte[] decryptedTextBytes = ProtectedData.Unprotect(encryptedTextBytes, encryptionSalt, DataProtectionScope.CurrentUser);
                return ASCIIEncoding.ASCII.GetString(decryptedTextBytes);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }
    }
}
