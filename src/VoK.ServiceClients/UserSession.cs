﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoK.ServiceClients
{
    public class UserSession
    {
        public UserSession(List<GameSubscription> subscriptions, string ticket, string username)
        {
            _gameSubscriptions = subscriptions;
            Ticket = ticket;
            Username = username;
        }

        private List<GameSubscription> _gameSubscriptions;

        public IReadOnlyList<GameSubscription> Subscriptions
        {
            get { return _gameSubscriptions; }
        }

        public string Ticket { get; private set; }

        public string Username { get; private set; }
    }
}
