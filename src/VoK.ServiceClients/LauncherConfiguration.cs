﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoK.ServiceClients
{
    public class LauncherConfiguration
    {
        public Dictionary<string, string> Settings { get; set; } = new Dictionary<string, string>();
    }
}
