﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoK.ServiceClients
{
    public class GameWorld
    {
        public string Name { get; private set; }

        public string LoginServerUrl { get; private set; }

        public string ChatServerUrl { get; private set; }

        public string StatusServerUrl { get; private set; }

        public int Order { get; private set; }

        public string Language { get; private set; }

        public GameWorld(string name, string loginServerUrl, string chatServerUrl, string statusServerUrl, int order, string language)
        {
            Name = name;
            LoginServerUrl = loginServerUrl;
            ChatServerUrl = chatServerUrl;
            StatusServerUrl = statusServerUrl;
            Order = order;
            Language = language;
        }
    }
}
